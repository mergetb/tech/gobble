module gitlab.com/mergetb/tech/gobble

require (
	github.com/fzipp/gocyclo v0.0.0-20150627053110-6acd4345c835 // indirect
	github.com/golang/protobuf v1.2.0
	github.com/mdlayher/netlink v0.0.0-20190617153422-f82a9b10b2bc
	github.com/mergetb/yaml v2.1.0+incompatible
	github.com/osrg/gobgp v0.0.0-20190801075736-fe95682cda71
	github.com/sirupsen/logrus v1.3.0
	gitlab.com/mergetb/tech/rtnl v0.1.8
	golang.org/x/lint v0.0.0-20181217174547-8f45f776aaf1 // indirect
	golang.org/x/sys v0.0.0-20190710143415-6ec70d6a5542
	google.golang.org/grpc v1.17.0
)

go 1.13
