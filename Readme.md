# Gobble: GoBGP Base Layer Engine

Gobble is a daemon that runs along side [GoBGP](https://github.com/osrg/gobgp) 
and updates the kernel routing and forwarding tables. It's a standalone daemon 
that just uses netlink and does not depend on Zebra. Notably, it has full EVPN 
support which was the reason the authors created it.

This is very much a WIP and we have tailored it to our specific use cases,
community contributions that cover other portions of the base packet layer are
more than welcome.

# Building

```shell
make
```

# Installation and Configuration Management

There is an Ansible role available here

- https://gitlab.com/mergetb/ansible/gobble
