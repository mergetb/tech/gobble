.PHONY: all
all: build/gobble

prefix ?= /usr/local

libsrc = $(addprefix pkg/,baselayer.go bgp.go ensure.go rtnetlink.go)
gobblesrc = $(addprefix cmd/gobble/,main.go)

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X main.version=$(VERSION)"

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

build/gobble: $(gobblesrc) $(libsrc) | build
	$(call go-build)

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -ldflags=${LDFLAGS} -o $@ $<
endef

.PHONY: clean
clean:
	$(QUIET) rm -rf build

build:
	$(QUIET) mkdir build

.PHONY: install
install: build/gobble download/gobgpd download/gobgp
	install -D build/gobble    $(DESTDIR)$(prefix)/bin/gobble
	install -D download/gobgpd $(DESTDIR)$(prefix)/bin/gobgpd
	install -D download/gobgp  $(DESTDIR)$(prefix)/bin/gobgp
	install -D gobble.yml $(DESTDIR)/etc/gobble.yml

download/gobgp_2.7.0_linux_amd64.tar.gz: | download
	curl -L \
		https://github.com/osrg/gobgp/releases/download/v2.7.0/gobgp_2.7.0_linux_amd64.tar.gz \
		-o download/gobgp_2.7.0_linux_amd64.tar.gz

download/gobgp download/gobgpd: download/gobgp_2.7.0_linux_amd64.tar.gz
	tar -C download -xzf download/gobgp_2.7.0_linux_amd64.tar.gz

download:
	mkdir -p download
