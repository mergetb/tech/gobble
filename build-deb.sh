#!/bin/bash

TARGET=${TARGET:-amd64}
DEBUILD_ARGS=${DEBUILD_ARGS:-""}

rm -f build/gobble*.build*
rm -f build/gobble*.change
rm -f build/gobble*.deb

debuild -e V=1 -e prefix=/usr $DEBUILD_ARGS -i -us -uc -b

mv ../gobble*.build* build/
mv ../gobble*.changes build/
mv ../gobble*.deb build/
