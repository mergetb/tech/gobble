#!/bin/bash

set -e

docker build -f debian/builder.dock -t gobble-builder .
docker run -v `pwd`:/gobble gobble-builder /gobble/build-deb.sh

