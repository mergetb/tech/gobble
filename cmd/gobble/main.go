package main

import (
	"flag"
	"io/ioutil"
	"time"

	"github.com/mergetb/yaml"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/rtnl"

	"gitlab.com/mergetb/tech/gobble/pkg"
)

var (
	version = "undefined"
)

func main() {

	log.Printf("gobble %s", version)

	configure()

	if gobble.Cfg.Trace {
		log.SetLevel(log.TraceLevel)
	} else {
		log.SetLevel(log.DebugLevel)
	}

	gobble.EnsureTableRules()

	for {
		exec()
		time.Sleep(time.Duration(gobble.Cfg.Quantum) * time.Millisecond)
	}

}

// apply default config items to any unconfigured parameters
func applyDefaultConfig() {

	if gobble.Cfg.Quantum == 0 {
		gobble.Cfg.Quantum = 1000
	}

	if gobble.Cfg.GobgpdPort == 0 {
		gobble.Cfg.GobgpdPort = 50051
	}

	if gobble.Cfg.PeerGw == "" {
		gobble.Cfg.PeerGw = "169.254.0.1"
	}

	if gobble.Cfg.BgpSubnet == "" {
		gobble.Cfg.BgpSubnet = "10.99.0.0/24"
	}

	if gobble.Cfg.Table == 0 {
		gobble.Cfg.Table = 47
	}

	if gobble.Cfg.PeerIfx == "" {

		// read the system links and use the first one
		// TODO we can be a bit smarter about this and read the FIB to see if we
		// any interfaces have a router neighbor and use that one.
		ctx, err := rtnl.OpenDefaultContext()
		if err != nil {
			log.Fatal("error opening netlink default context: %v", err)
		}

		links, err := rtnl.ReadLinks(ctx, nil)
		if err != nil {
			log.Fatal("error reading system interfaces: %v", err)
		}
		if len(links) == 0 {
			log.Fatal("system has no interfaces")
		}

		gobble.Cfg.PeerIfx = links[0].Info.Name

	}

}

func configure() {

	var (
		quantum    = flag.Int("quantum", 0, "millisecond sleep quantum between runs")
		gobgpdPort = flag.Int("gobgpd-port", 0, "gobgpd listening port")
		trace      = flag.Bool("trace", false, "enable trace level log messages")
		table      = flag.Uint("table", 0, "routing table to use")
		peerifx    = flag.String("peer-interface", "", "interface connected to peer router")
		peerGw     = flag.String("peer-gateway", "", "peer gateway ip")
		bgpSubnet  = flag.String("bgp-subnet", "", "bgp subnet")
		config     = flag.String("config", "/etc/gobble.yml", "gobble configuration")
	)

	// try to read the config file
	cfg, err := ioutil.ReadFile(*config)
	if err != nil {
		log.Warnf("error reading config %s: %v", *config, err)
	} else {
		err = yaml.Unmarshal(cfg, &gobble.Cfg)
		if err != nil {
			log.Warnf("error parsing config %s: %v", *config, err)
		}
	}

	applyDefaultConfig()

	// parse flags and update config accordingly, note that flags take precedence
	// over the config file.
	flag.Parse()

	if *quantum != 0 {
		gobble.Cfg.Quantum = *quantum
	}
	if *peerifx != "" {
		gobble.Cfg.PeerIfx = *peerifx
	}
	if *gobgpdPort != 0 {
		gobble.Cfg.GobgpdPort = *gobgpdPort
	}
	if *peerGw != "" {
		gobble.Cfg.PeerGw = *peerGw
	}
	if *bgpSubnet != "" {
		gobble.Cfg.BgpSubnet = *bgpSubnet
	}
	if *trace {
		gobble.Cfg.Trace = *trace
	}
	if *table != 0 {
		gobble.Cfg.Table = *table
	}
}

func exec() {

	bgpState, err := gobble.ReadBgp()
	if err != nil {
		log.WithError(err).Error("read-bgp failed")
		return
	}

	baselayerState, err := gobble.ReadBaselayer()
	if err != nil {
		log.WithError(err).Error("read-baselayer failed")
		return
	}

	gobble.Ensure(bgpState, baselayerState)

}
