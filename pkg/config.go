package gobble

// Cfg is the global gobble config
var Cfg Config

// Config contains all the configuration information needed for Gobble to run.
type Config struct {
	Oif        uint32 `yaml:"oif"`
	Quantum    int    `yaml:"quantum"`
	GobgpdPort int    `yaml:"gobgpd_port"`
	PeerIfx    string `yaml:"peer_ifx"`
	PeerGw     string `yaml:"peer_gw"`
	BgpSubnet  string `yaml:"bgp_subnet"`
	Trace      bool   `yaml:"trace"`
	Table      uint   `yaml:"table"`
}
