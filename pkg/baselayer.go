package gobble

import (
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"
)

// BaselayerState keeps track of state in the Linux packet layer
type BaselayerState struct {
	DestRT       map[string]Route // Destination keyed routing table
	Fib          []Neighbor
	Neighborhood []Neighbor
	Links        map[uint32]*Link
}

// IsLocalIP determines whether or not an IP address is local to the machine
// Gobble is running on.
func (b BaselayerState) IsLocalIP(ip string) bool {
	for _, l := range b.Links {
		for _, a := range l.IPAddrs {
			if a.String() == ip {
				return true
			}
		}
	}
	return false
}

// Route encapsulates information about a route that is important to Gobble
type Route struct {
	Dest     net.IP
	Src      net.IP
	Gateway  net.IP
	PrefSrc  net.IP
	Oif      uint32
	Iif      uint32
	Priority uint32
	Metrics  uint32
	Family   uint8
}

// Key is an indexing function for routes. This function is used to determine
// the uniqueness of a route relative to Gobble operations.
func (r Route) Key() string {
	return fmt.Sprintf("%s~%s~%d", r.Dest, r.Gateway, r.Oif)
}

// Neighbor encapsulates information about neighbors that is important to Gobble
type Neighbor struct {
	Mac    net.HardwareAddr
	Vlan   uint32
	Port   uint32
	Master uint32
	If     uint32
	Ifx    string
	Dst    net.IP
	Vni    uint32
	SrcVni uint32
	Router bool
	Family uint8
	Nsid   uint32
}

// Key is an indexing function for neighbors. This function is used to
// determine the uniqueness of a neighbor relative to Gobble operations.
func (f Neighbor) Key() string {
	return fmt.Sprintf("%s~%d~%s", f.Mac, f.If, f.Dst)
}

// Link encapsulates information about links that is important to Gobble
type Link struct {
	Name    string
	Index   uint32
	Vni     uint32
	IPAddrs []net.IP
}

// ReadBaselayer reads the underlying routing, neighbor and forwarding tables
// from the Linux kernel.
func ReadBaselayer() (*BaselayerState, error) {

	routes, err := readRoutes()
	if err != nil {
		return nil, err
	}

	links, err := readLinks()
	if err != nil {
		return nil, err
	}

	log.Trace("read fib")
	fib, err := readNeighbors(unix.AF_BRIDGE)
	if err != nil {
		return nil, err
	}

	log.Trace("read neighborhood")
	hood, err := readNeighbors(unix.AF_UNSPEC)
	if err != nil {
		return nil, err
	}

	return &BaselayerState{routes, fib, hood, links}, err

}

// GetPeerMac finds the MAC address of our link level peer router.
// TODO Right now we assume there is only 1.
func (b *BaselayerState) GetPeerMac() (net.HardwareAddr, error) {

	oif := b.GetPeerOif()
	for _, n := range b.Neighborhood {
		if n.If == oif && n.Router {
			return n.Mac, nil
		}
	}

	return nil, fmt.Errorf("not found")

}

// GetPeerOif finds the interface our link level peer router is on.
// TODO right now assume there is only 1.
func (b *BaselayerState) GetPeerOif() uint32 {

	if Cfg.Oif != 0 {
		return Cfg.Oif
	}

	for _, l := range b.Links {
		if l.Name == Cfg.PeerIfx {
			Cfg.Oif = l.Index
		}
	}

	return Cfg.Oif

}

func findVtep(vni uint32, state *BaselayerState) (*Link, bool) {

	for _, l := range state.Links {

		if l.Vni == vni {
			return l, true
		}

	}

	return nil, false

}

func isGobbleEntry(e Neighbor) bool {

	// gobble currently handles the following cases
	//   - any neighbor that smells like EVPN (has a VNI tag)
	//   - any neighbor whose dst points to something (not a simple bridge member)
	//   - forwarding entry for bgp
	return (e.Vni != 0 && e.Dst != nil) || e.Dst.String() == Cfg.PeerGw

}
