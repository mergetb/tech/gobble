package gobble

import (
	"encoding/binary"
	"fmt"
	"net"

	"github.com/mdlayher/netlink"
	log "github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"
)

/*---------------------------------------------------------------------------*\
 * rtnetlink support
 * .................
 *
 * This file contains a set of functions and types to interact with Linux
 * netlink. There are three basic categories of things.
 *  - routes
 *  - neighbors
 *  - links
 *
 * Each category contains
 *  - functions for reading the state of objects within the category
 *  - functions for setting the state of objects within the category
 *  - data structures to facilitate netlink i/o + marshal/unmarshal funcs
\*---------------------------------------------------------------------------*/

// routes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// RtMsg encapsulates a unix.RtMsg providing marshal/unmarshal operations and
// Gobble-salient property extraction.
type RtMsg struct {
	Msg           unix.RtMsg
	RawAttributes []netlink.Attribute

	// extracted attribures
	Route
}

// Marshal a route message to bytes
func (rtm RtMsg) Marshal() ([]byte, error) {

	m := rtm.Msg

	flags := make([]byte, 4)
	binary.LittleEndian.PutUint32(flags, m.Flags)

	oif := make([]byte, 4)
	binary.LittleEndian.PutUint32(oif, rtm.Oif)

	message := []byte{
		m.Family,
		m.Dst_len,
		m.Src_len,
		m.Tos,
		m.Table,
		m.Protocol,
		m.Scope,
		m.Type,
		flags[0], flags[1], flags[2], flags[3],
	}

	attrs := []netlink.Attribute{
		// destination
		{
			Length: 4 + 4,
			Type:   unix.RTA_DST,
			Data:   rtm.Dest.To4(),
		},
		// gateway
		{
			Length: 4 + 4,
			Type:   unix.RTA_GATEWAY,
			Data:   rtm.Gateway.To4(),
		},
		// outbound interface
		{
			Length: 4 + 4,
			Type:   unix.RTA_OIF,
			Data:   oif,
		},
	}

	if rtm.Route.Metrics != 0 {

		metrics := make([]byte, 4)
		binary.LittleEndian.PutUint32(metrics, rtm.Route.Metrics)

		attrs = append(attrs, netlink.Attribute{
			Length: 4 + 4,
			Type:   unix.RTA_METRICS,
			Data:   metrics,
		})

	}
	//TODO add other known attributes if not zero values

	//attrs = append(attrs, rtm.RawAttributes...)

	attributes, err := netlink.MarshalAttributes(attrs)
	if err != nil {
		return nil, err
	}

	return append(message, attributes...), nil
}

// Unmarshal an route message and its attributes from bytes
func (rtm *RtMsg) Unmarshal(bs []byte) error {

	flags := binary.LittleEndian.Uint32(bs[8:12])

	rtm.Msg = unix.RtMsg{
		Family:   bs[0],
		Dst_len:  bs[1],
		Src_len:  bs[2],
		Tos:      bs[3],
		Table:    bs[4],
		Protocol: bs[5],
		Scope:    bs[6],
		Type:     bs[7],
		Flags:    flags,
	}

	ad, err := netlink.NewAttributeDecoder(bs[12:])
	if err != nil {
		log.WithError(err).Error("error creating decoder")
		return err
	}

	for ad.Next() {
		switch ad.Type() {

		case unix.RTA_DST:
			rtm.Dest = net.IP(ad.Bytes())
		case unix.RTA_SRC:
			rtm.Src = net.IP(ad.Bytes())
		case unix.RTA_GATEWAY:
			rtm.Gateway = net.IP(ad.Bytes())
		case unix.RTA_PREFSRC:
			rtm.PrefSrc = net.IP(ad.Bytes())
		case unix.RTA_IIF:
			rtm.Iif = ad.Uint32()
		case unix.RTA_OIF:
			rtm.Oif = ad.Uint32()
		case unix.RTA_PRIORITY:
			rtm.Priority = ad.Uint32()
		case unix.RTA_METRICS:
			rtm.Metrics = ad.Uint32()

		default:
			rtm.RawAttributes = append(rtm.RawAttributes, netlink.Attribute{
				Length: uint16(len(ad.Bytes()) + 4), // +4 for length & type fields
				Type:   ad.Type(),
				Data:   ad.Bytes(),
			})
		}
	}

	return nil

}

// read all routes from netlink returning a map that is keyed based on the
// route destination
func readRoutes() (map[string]Route, error) {

	conn, err := netlink.Dial(unix.NETLINK_ROUTE, nil)
	if err != nil {
		log.WithError(err).Error("failed to dial netlink")
		return nil, err
	}
	defer conn.Close()

	rtmsg, err := RtMsg{
		Msg: unix.RtMsg{
			Family: unix.AF_INET,
			Table:  uint8(Cfg.Table),
		},
	}.Marshal()
	if err != nil {
		log.WithError(err).Error("failed to marshal rtmsg")
		return nil, err
	}

	m := netlink.Message{
		Header: netlink.Header{
			Type: unix.RTM_GETROUTE,
			Flags: netlink.Request |
				netlink.Atomic |
				netlink.Root,
		},
		Data: rtmsg,
	}

	resp, err := conn.Execute(m)
	if err != nil {
		return nil, err
	}

	log.Tracef("current baselayer routes (%d)", len(resp))

	rts := make(map[string]Route)
	for _, r := range resp {

		var rtm RtMsg
		rtm.Unmarshal(r.Data)

		log.WithFields(log.Fields{
			"dest":    rtm.Route.Dest,
			"Gateway": rtm.Route.Gateway,
			"Oif":     rtm.Route.Oif,
			"Table":   rtm.Msg.Table,
		}).Trace("route")

		rtm.Route.Family = rtm.Family
		rts[rtm.Route.Dest.String()] = rtm.Route

	}

	return rts, nil

}

// AddRoutes adds the supplied set of routes to the Linux routing tables
func AddRoutes(rs []Route) error {

	return modifyRoutes(rs, unix.RTM_NEWROUTE)

}

// RemoveRoutes removes the supplied set of routes from the Linux routing tables
func RemoveRoutes(rs []Route) error {

	return modifyRoutes(rs, unix.RTM_DELROUTE)

}

// modify a set of routes
// op=RTM_NEWROUTE ---> add
// op=RTM_DELROUTE ---> remove
func modifyRoutes(rs []Route, op uint16) error {

	// prepare netlink messages
	var messages []netlink.Message

	flags := netlink.Request | netlink.Acknowledge
	if op == unix.RTM_NEWROUTE {
		flags |= netlink.Create | netlink.Append
	}

	for _, r := range rs {

		fields := log.Fields{
			"key":   r.Key(),
			"op":    op,
			"route": fmt.Sprintf("%+v", r),
		}

		switch op {

		case unix.RTM_NEWROUTE:
			log.WithFields(fields).Trace("adding route")

		case unix.RTM_DELROUTE:
			log.WithFields(fields).Trace("removing route")

		default:
			log.WithFields(fields).Warning("unsupported route operation, skipping")
			continue

		}

		r.Metrics = 20 //default for BGP

		msg := RtMsg{
			Msg: unix.RtMsg{
				Family:   unix.AF_INET, // r.Family, //unix.AF_INET,
				Table:    uint8(Cfg.Table),
				Type:     unix.RTN_UNICAST,
				Protocol: unix.RTPROT_BGP,
				Scope:    unix.RT_SCOPE_UNIVERSE,
				Flags:    unix.RTNH_F_ONLINK,
				Dst_len:  32,
			},
			Route: r,
		}

		data, err := msg.Marshal()
		if err != nil {
			log.WithError(err).Error("failed to marshal rtmsg")
			return err
		}

		m := netlink.Message{
			Header: netlink.Header{
				Type:  netlink.HeaderType(op),
				Flags: flags,
			},
			Data: data,
		}

		messages = append(messages, m)

	}

	// send netlink messages
	return netlinkUpdate(messages)

}

// neighbors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// NdMsg is a Netlink message for communicating with the kernel about
// neighbors. The unix library does not have this one
type NdMsg struct {
	Family  uint8
	Ifindex uint32
	State   uint16
	Flags   uint8
	Type    uint8
}

// attribute types
const (
	NDAUnspec uint16 = iota
	NDADst
	NDALLAddr
	NDACacheInfo
	NDAProbes
	NDAVLAN
	NDAPort
	NDAVNI
	NDAIfIndex
	NDAMaster
	NDALinkNetnsID
	NDASrcVNI
)

// neighbor cache entry flags
const (
	NTFUse        = 0x01
	NTFSelf       = 0x02
	NTFMaster     = 0x04
	NTFProxy      = 0x08
	NTFExtLearned = 0x10
	NTFOffloaded  = 0x20
	NTFRouter     = 0x80
)

// neighbor cache entry states
const (
	NUDIncomplete = 0x01
	NUDReachable  = 0x02
	NUDStale      = 0x04
	NUDDelay      = 0x08
	NUDProbe      = 0x10
	NUDFailed     = 0x20
	NUDNoarp      = 0x40
	NUDPermanent  = 0x80
	NUDNone       = 0x00
)

// NbrMsg encapsulates a netlink NdMsg, providing Marshal/Unmarshal support and
// Gobble-salient property extraction.
type NbrMsg struct {
	Msg           NdMsg
	RawAttributes []netlink.Attribute

	Neighbor
}

// Marshal a neighbor message to bytes
func (n NbrMsg) Marshal() ([]byte, error) {

	ifindex := make([]byte, 4)
	binary.LittleEndian.PutUint32(ifindex, n.Msg.Ifindex)

	state := make([]byte, 2)
	binary.LittleEndian.PutUint16(state, n.Msg.State)

	message := []byte{
		n.Msg.Family,
		0, 0, 0, //padding per include/uapi/linux/neighbour.h
		ifindex[0], ifindex[1], ifindex[2], ifindex[3],
		state[0], state[1],
		n.Msg.Flags,
		n.Msg.Type,
	}

	attrs := []netlink.Attribute{}

	// mac
	if n.Neighbor.Mac != nil {
		attrs = append(attrs, netlink.Attribute{
			Length: 6 + 4,
			Type:   NDALLAddr,
			Data:   n.Neighbor.Mac,
		})
	}

	// dst
	if n.Neighbor.Dst != nil {
		attrs = append(attrs, netlink.Attribute{
			Length: 4 + 4,
			Type:   NDADst,
			Data:   n.Neighbor.Dst.To4(),
		})
	}

	//TODO add other known attributes

	attributes, err := netlink.MarshalAttributes(attrs)
	if err != nil {
		return nil, err
	}

	return append(message, attributes...), nil

}

// Unmarshal a neighbor message and its attributes from bytes
func (n *NbrMsg) Unmarshal(bs []byte) error {

	ifindex := binary.LittleEndian.Uint32(bs[4:8])
	state := binary.LittleEndian.Uint16(bs[8:10])

	n.Msg = NdMsg{
		Family:  bs[0],
		Ifindex: ifindex,
		State:   state,
		Flags:   bs[10],
		Type:    bs[11],
	}

	n.Neighbor.Router = (n.Msg.Flags&NTFRouter) != 0 && (n.Msg.State&NUDReachable) != 0
	n.Neighbor.If = n.Msg.Ifindex

	ad, err := netlink.NewAttributeDecoder(bs[12:])
	if err != nil {
		log.WithError(err).Error("error creating decoder")
		return err
	}

	for ad.Next() {
		switch ad.Type() {

		case NDADst:
			n.Neighbor.Dst = net.IP(ad.Bytes())
		case NDALLAddr:
			n.Neighbor.Mac = net.HardwareAddr(ad.Bytes())
		case NDAVLAN:
			n.Neighbor.Vlan = ad.Uint32()
		case NDAPort:
			n.Neighbor.Port = ad.Uint32()
		case NDAVNI:
			n.Neighbor.Vni = ad.Uint32()
		case NDAIfIndex:
			n.Neighbor.If = ad.Uint32()
		case NDAMaster:
			n.Neighbor.Master = ad.Uint32()
		case NDASrcVNI:
			n.Neighbor.SrcVni = ad.Uint32()
		case NDALinkNetnsID:
			n.Neighbor.Nsid = ad.Uint32()

		case NDAUnspec, NDACacheInfo, NDAProbes:
			fallthrough
		default:
			n.RawAttributes = append(n.RawAttributes, netlink.Attribute{
				Length: uint16(len(ad.Bytes()) + 4), // +4 for length & type fields
				Type:   ad.Type(),
				Data:   ad.Bytes(),
			})

		}
	}

	return nil

}

// read the forwarding database (FDB), this essentially means reading all the
// neighbors in the AF_BRIDGE family
func readNeighbors(family uint8) ([]Neighbor, error) {

	conn, err := netlink.Dial(unix.NETLINK_ROUTE, nil)
	if err != nil {
		log.WithError(err).Error("failed to dial netlink")
		return nil, err
	}
	defer conn.Close()

	// XXX working around this
	// https://lkml.org/lkml/2018/10/16/1407
	//
	// tl;dr when dumping bridge neighbors (AF_BRIDGE) we need to send netlink
	// an IfInfomsg, when dumping other types of neighbors (AF_INET[6], AF_UNSPEC)
	// we need to send netlink an NdMsg
	var data []byte
	if family == unix.AF_BRIDGE {
		data = marshalIfinfoMsg(unix.IfInfomsg{Family: family})
	} else {
		data, err = NbrMsg{Msg: NdMsg{Family: family}}.Marshal()
		if err != nil {
			return nil, err
		}
	}

	m := netlink.Message{
		Header: netlink.Header{
			Type: unix.RTM_GETNEIGH,
			Flags: netlink.Request |
				netlink.Atomic |
				netlink.Root,
		},
		Data: data,
	}

	resp, err := conn.Execute(m)
	if err != nil {
		return nil, err
	}

	log.Tracef("current baselayer neighbors (%d)", len(resp))

	var nbs []Neighbor
	for _, r := range resp {

		var m NbrMsg
		err := m.Unmarshal(r.Data)
		if err != nil {
			return nil, err
		}

		log.WithFields(log.Fields{
			"mac":    m.Neighbor.Mac.String(),
			"dst":    m.Neighbor.Dst.String(),
			"if":     m.Neighbor.If,
			"ifx":    m.Neighbor.Ifx,
			"master": m.Neighbor.Master,
			"router": m.Neighbor.Router,
			"netns":  m.Neighbor.Nsid,
		}).Trace("neighbor")

		m.Neighbor.Family = m.Family //family
		if m.Neighbor.Family == unix.AF_UNSPEC {
			m.Neighbor.Family = unix.AF_INET
		}
		if family == unix.AF_BRIDGE {
			m.Neighbor.Family = unix.AF_BRIDGE
		}
		nbs = append(nbs, m.Neighbor)
	}

	return nbs, nil

}

// AddNeighbors adds the provided set of neighbors to the Linux kernel
func AddNeighbors(ns []Neighbor) error {

	return modifyNeighbors(ns, unix.RTM_NEWNEIGH)

}

// RemoveNeighbors removes the provided set of neighbors from the Linux kernel
func RemoveNeighbors(ns []Neighbor) error {

	return modifyNeighbors(ns, unix.RTM_DELNEIGH)

}

func modifyNeighbors(ns []Neighbor, op uint16) error {

	// prepare netlink messages
	var messages []netlink.Message

	flags := netlink.Request | netlink.Acknowledge
	if op == unix.RTM_NEWNEIGH {
		flags |= netlink.Create | netlink.Append
	}

	for _, n := range ns {

		fields := log.Fields{
			"key":    n.Key(),
			"family": n.Family,
			"op":     op,
			"if":     n.If,
			"nbr":    fmt.Sprintf("%#v", n),
		}

		switch op {

		case unix.RTM_NEWNEIGH:
			log.WithFields(fields).Trace("adding neighbor")
			flags |= netlink.Create | netlink.Append

		case unix.RTM_DELNEIGH:
			log.WithFields(fields).Trace("removing neighbor")

		default:
			log.WithFields(fields).Warning("unsupported neighbor operation, skipping")
		}

		msg := NbrMsg{
			Msg: NdMsg{
				Family:  n.Family,
				Ifindex: n.If,
			},
		}

		msg.Msg.State |= NUDPermanent

		if op == unix.RTM_DELNEIGH {

			msg.Msg.Flags |= NTFSelf
			msg.Family = unix.AF_BRIDGE
			msg.Neighbor = Neighbor{
				Mac: n.Mac,
				If:  n.If,
				Dst: n.Dst,
			}

		} else {

			msg.Neighbor = n
			if n.Family == unix.AF_UNSPEC {
				msg.Msg.State |= NUDReachable
			}
			if n.Family == unix.AF_BRIDGE {
				msg.Msg.Flags |= NTFSelf
			}

		}

		data, err := msg.Marshal()
		if err != nil {
			log.WithError(err).Error("failed to marshal ndmsg")
			return err
		}

		m := netlink.Message{
			Header: netlink.Header{
				Type:  netlink.HeaderType(op),
				Flags: flags,
			},
			Data: data,
		}

		messages = append(messages, m)

	}

	// send netlink messages
	return netlinkUpdate(messages)

}

// TODO(ry) wrap this up in a package-local structure and provide same
// Marshal/Unmarshal interface as the rest of the netlink messages
//
// marshal an interface info message to bytes
func marshalIfinfoMsg(m unix.IfInfomsg) []byte {

	typ := make([]byte, 2)
	binary.LittleEndian.PutUint16(typ, m.Type)

	index := make([]byte, 4)
	binary.LittleEndian.PutUint32(index, uint32(m.Index))

	flags := make([]byte, 4)
	binary.LittleEndian.PutUint32(flags, m.Flags)

	change := make([]byte, 4)
	binary.LittleEndian.PutUint32(change, m.Change)

	return []byte{
		m.Family,
		0, //padding per include/uapi/linux/rtnetlink.h
		typ[0], typ[1],
		index[0], index[1], index[2], index[3],
		flags[0], flags[1], flags[2], flags[3],
		change[0], change[1], change[2], change[3],
	}

}

// TODO(ry) wrap this up in a package-local structure and provide same
// Marshal/Unmarshal interface as the rest of the netlink messages
//
// unmarshal an interface info message an its attributes from bytes
func unmarshalIfinfoMsg(bs []byte) (unix.IfInfomsg, []byte) {

	typ := binary.LittleEndian.Uint16(bs[2:4])
	index := binary.LittleEndian.Uint32(bs[4:8])
	flags := binary.LittleEndian.Uint32(bs[8:12])
	change := binary.LittleEndian.Uint32(bs[12:16])

	msg := unix.IfInfomsg{
		Family: bs[0],
		Type:   typ,
		Index:  int32(index),
		Flags:  flags,
		Change: change,
	}
	return msg, bs[16:]

}

// links ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// interface link address attribute types
const (
	IFLAInfoUnspec uint16 = iota
	IFLAInfoKind
	IFLAInfoData
)

// vxlan attribute types
const (
	IFLAVXLANUnspec uint16 = iota
	IFLAVXLANID
)

// TODO(ry) wrap this up in a package-local structure and provide same
// Marshal/Unmarshal interface as the rest of the netlink messages
//
// marshal an interface address message to bytes
func marshalIfaddrMsg(m unix.IfAddrmsg) []byte {

	index := make([]byte, 4)
	binary.LittleEndian.PutUint32(index, m.Index)

	return []byte{
		m.Family,
		m.Prefixlen,
		m.Flags,
		m.Scope,
		index[0], index[1], index[2], index[3],
	}

}

// TODO(ry) wrap this up in a package-local structure and provide same
// Marshal/Unmarshal interface as the rest of the netlink messages
//
// unmarshal an interface address message and its attributes
func unmarshalIfaddrMsg(bs []byte) (unix.IfAddrmsg, []byte) {

	index := binary.LittleEndian.Uint32(bs[4:8])

	msg := unix.IfAddrmsg{
		Family:    bs[0],
		Prefixlen: bs[1],
		Flags:     bs[2],
		Scope:     bs[3],
		Index:     index,
	}

	return msg, bs[8:]
}

// read links information from netlink, returning a map of link objects keyed on
// the interface number. it's not the case that links are monotonically
// increasing in index, so the map is required.
func readLinks() (map[uint32]*Link, error) {

	conn, err := netlink.Dial(unix.NETLINK_ROUTE, nil)
	if err != nil {
		log.WithError(err).Error("failed to dial network")
		return nil, err
	}
	defer conn.Close()

	m := netlink.Message{
		Header: netlink.Header{
			Type: unix.RTM_GETLINK,
			Flags: netlink.Request |
				netlink.Atomic |
				netlink.Root,
		},
		Data: marshalIfinfoMsg(unix.IfInfomsg{
			Family: unix.AF_INET,
		}),
	}

	resp, err := conn.Execute(m)
	if err != nil {
		return nil, err
	}
	log.Tracef("current baselayer links (%d)", len(resp))

	lks := make(map[uint32]*Link)
	for _, r := range resp {

		m, attrs := unmarshalIfinfoMsg(r.Data)
		if err != nil {
			log.WithError(err).Error("error reading ifinfo")
			return nil, err
		}

		lnk := &Link{
			Index: uint32(m.Index),
		}
		err := lnk.Unmarshal(attrs)
		if err != nil {
			continue
		}

		lks[lnk.Index] = lnk
	}

	// now that we have the links, go get their addresses
	err = readLinkAddrs(lks)
	if err != nil {
		return nil, err
	}

	for _, l := range lks {
		log.WithFields(log.Fields{
			"index":   l.Index,
			"name":    l.Name,
			"vni":     l.Vni,
			"ipaddrs": fmt.Sprintf("%+v", l.IPAddrs),
		}).Trace("link info")
	}

	return lks, nil

}

// Unmarshal a link from attributes, extracting Gobble-salient properties.
func (lnk *Link) Unmarshal(attrs []byte) error {

	ad, err := netlink.NewAttributeDecoder(attrs)
	if err != nil {
		log.WithError(err).Error("error creating decoder")
		return err
	}

	// keep track of the current attribute kind, to get down to the vxlan
	// attributes we have to spelunk through a few layers of attributes, most of
	// the nested attribute types we don't care about and don't need to recurse
	// down into. because of the sequential read nature of interacting with
	// netlink attribute sets, we need to keep track of what attribute we are
	// currently at and look back later when we hit an IFLA_INFO_DATA attribute
	// to see if it's a data attribute we need to recurse into
	var currentKind string
	for ad.Next() {
		switch ad.Type() {

		case unix.IFLA_IFNAME:
			lnk.Name = ad.String()

		case unix.IFLA_LINKINFO:

			// always dive into linkinfo
			nad, err := netlink.NewAttributeDecoder(ad.Bytes())
			if err != nil {
				log.WithError(err).Warning("failed to create nested decoder")
				continue
			}
			for nad.Next() {
				switch nad.Type() {

				// keep track of the current attribute kind
				case IFLAInfoKind:
					currentKind = nad.String()

				case IFLAInfoData:
					// only interested in vxlan things at the moment, if we hit a data
					// attribute and were in a vxlan context, dive in
					if currentKind != "vxlan" {
						continue
					}
					nnad, err := netlink.NewAttributeDecoder(nad.Bytes())
					if err != nil {
						log.WithError(err).Warning("failed to create 2x nested decoder")
						continue
					}

					// iterate through the vxlan info attributes
					for nnad.Next() {
						switch nnad.Type() {

						case IFLAVXLANID:
							// w00t, found the VNI
							lnk.Vni = nnad.Uint32()

						}
					}

				}
			}

		}
	}

	// should not happen
	if lnk.Name == "" {

		log.WithFields(log.Fields{
			"index": lnk.Index,
		}).Warn("link has no name - this is probably a bug")

		return fmt.Errorf("no link name")

	}

	return nil

}

// ask netlink for all addresses it knows about and update the provided set of
// links with addresses sets matched over link (interface) index.
func readLinkAddrs(links map[uint32]*Link) error {

	conn, err := netlink.Dial(unix.NETLINK_ROUTE, nil)
	if err != nil {
		log.WithError(err).Error("failed to dial network")
		return err
	}
	defer conn.Close()

	m := netlink.Message{
		Header: netlink.Header{
			Type: unix.RTM_GETADDR,
			Flags: netlink.Request |
				netlink.Atomic |
				netlink.Root,
		},
		Data: marshalIfaddrMsg(unix.IfAddrmsg{
			Family: unix.AF_INET,
		}),
	}

	resp, err := conn.Execute(m)
	if err != nil {
		return err
	}

	log.Tracef("link addresses (%d)", len(resp))

	for _, r := range resp {

		m, attrs := unmarshalIfaddrMsg(r.Data)
		if err != nil {
			log.WithError(err).Error("error reading ifaddr")
			return err
		}

		ad, err := netlink.NewAttributeDecoder(attrs)
		if err != nil {
			log.WithError(err).Error("error creating decoder")
			return err
		}

		for ad.Next() {
			switch ad.Type() {

			case unix.IFA_ADDRESS:

				// try to get a link from the provided map that matches the interface
				// index returned by netlink, if found add the address to that links
				// address list
				link, ok := links[m.Index]
				if !ok {
					log.WithFields(log.Fields{
						"index": m.Index,
					}).Warning("unknown interface index")
					continue
				}
				link.IPAddrs = append(link.IPAddrs, net.IP(ad.Bytes()))

			}
		}

	}

	return nil

}

// helper functions -----------------------------------------------------------

func withNetlink(f func(*netlink.Conn) error) error {

	conn, err := netlink.Dial(unix.NETLINK_ROUTE, nil)
	if err != nil {
		log.WithError(err).Error("failed to dial netlink")
		return err
	}
	defer conn.Close()

	return f(conn)

}

func netlinkUpdate(messages []netlink.Message) error {
	return withNetlink(func(c *netlink.Conn) error {

		for _, m := range messages {

			resp, err := c.Execute(m)
			if err != nil {
				log.WithError(err).Error("netlink call failed")
				return err
			}

			for _, r := range resp {

				if r.Header.Type == netlink.Error {

					code := binary.LittleEndian.Uint32(r.Data[0:4])

					if code == 0 {
						log.Trace("netlink update acknowledged")
					} else {
						log.WithFields(log.Fields{
							"code": code,
						}).Warn("netlink update failed")
						return fmt.Errorf(string(r.Data))
					}

				}
			}

		}

		return nil

	})
}
