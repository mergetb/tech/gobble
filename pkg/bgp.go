package gobble

import (
	"context"
	"fmt"
	"io"

	"github.com/golang/protobuf/ptypes"
	api "github.com/osrg/gobgp/api"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

// Type2EvpnRoute encapsulates a MAC advertisement route with a set of next-hops
type Type2EvpnRoute struct {
	Route    *api.EVPNMACIPAdvertisementRoute
	NextHops []string
}

// Type3EvpnRoute encapsulates a multicast Ethernet tag route with a community
// local admin tag
type Type3EvpnRoute struct {
	Route               *api.EVPNInclusiveMulticastEthernetTagRoute
	CommunityLocalAdmin uint32
}

// BgpState collects Type2Routes and Type3Routes in one place
type BgpState struct {
	Type2Routes []Type2EvpnRoute
	Type3Routes []Type3EvpnRoute
}

// reach out to gobgpd through grpc to get the global routing information base
// (rib)
func ReadBgp() (*BgpState, error) {

	conn, client, err := dial()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	// list EVPN paths according to the EVPN AFI/SAFI combo
	stream, err := client.ListPath(context.Background(), &api.ListPathRequest{
		TableType: api.TableType_GLOBAL,
		Family: &api.Family{
			Afi:  api.Family_AFI_L2VPN,
			Safi: api.Family_SAFI_EVPN,
		},
		SortType: api.ListPathRequest_PREFIX,
	})
	if err != nil {
		log.WithError(err).Error("bgp list-path failed")
		return nil, err
	}

	// iterate through the results, collecting in a list
	var rib []*api.Destination
	for {
		r, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			log.WithError(err).Error("bgp list-path: broken stream")
			return nil, err
		}
		rib = append(rib, r.Destination)
	}

	// process results
	return processRIB(rib)

}

func processType2Routes(result *BgpState, p *api.Path) error {

	// Type 2 routes
	if ptypes.Is(p.Nlri, &api.EVPNMACIPAdvertisementRoute{}) {

		// extract the type-2 route entry
		m := &api.EVPNMACIPAdvertisementRoute{}
		err := ptypes.UnmarshalAny(p.Nlri, m)
		if err != nil {
			log.WithError(err).Error("failed to parse type 2 route")
			return err
		}

		// collect the next-hops from the path attributes
		var nexthops []string
		for _, p := range p.Pattrs {
			if ptypes.Is(p, &api.MpReachNLRIAttribute{}) {
				a := &api.MpReachNLRIAttribute{}
				err := ptypes.UnmarshalAny(p, a)
				if err != nil {
					log.WithError(err).Error("failed to parse mp-reach-nlri-attr")
					return err
				}
				nexthops = a.NextHops
			}
		}

		// package up type-2 route and next-hops and add to result
		result.Type2Routes = append(result.Type2Routes, Type2EvpnRoute{m, nexthops})
	}

	return nil

}

func processType3Routes(result *BgpState, p *api.Path) error {

	if ptypes.Is(p.Nlri, &api.EVPNInclusiveMulticastEthernetTagRoute{}) {

		// extract the type-3 route entry
		m := &api.EVPNInclusiveMulticastEthernetTagRoute{}
		err := ptypes.UnmarshalAny(p.Nlri, m)
		if err != nil {
			log.WithError(err).Error("failed to parse type 3 route")
			return err
		}

		// will hold the VNI label once we extract it
		var label uint32

		// find the VNI label in the attributes, this is a nested attribute
		// found at
		//
		// ExtendedCommunityAttributes.Communities[TwoOctetAsSpecificExtended]
		for _, pa := range p.Pattrs {

			if ptypes.Is(pa, &api.ExtendedCommunitiesAttribute{}) {

				xa := &api.ExtendedCommunitiesAttribute{}
				err := ptypes.UnmarshalAny(pa, xa)
				if err != nil {
					log.WithError(err).Error("failed to parse extended communities")
					return err
				}

				for _, ppa := range xa.Communities {

					if ptypes.Is(ppa, &api.TwoOctetAsSpecificExtended{}) {

						a := &api.TwoOctetAsSpecificExtended{}
						err := ptypes.UnmarshalAny(ppa, a)
						if err != nil {
							log.WithError(err).Error(
								"Failed to parse AS specific extended attr")
							return err
						}

						// found it
						label = a.LocalAdmin

					}

				}
			}
		}

		result.Type3Routes = append(result.Type3Routes, Type3EvpnRoute{m, label})
	}

	return nil

}

func processRIB(rib []*api.Destination) (*BgpState, error) {

	result := &BgpState{}
	for _, d := range rib {

		log.Trace(d.Prefix)

		for _, p := range d.Paths {

			err := processType2Routes(result, p)
			if err != nil {
				return nil, err
			}

			err = processType3Routes(result, p)
			if err != nil {
				return nil, err
			}

		}
	}

	log.Trace("current BGP state")
	for _, x := range result.Type2Routes {
		log.WithFields(log.Fields{
			"mac":       x.Route.MacAddress,
			"labels":    x.Route.Labels,
			"next-hops": x.NextHops,
		}).Trace("type-2 route")
	}

	for _, x := range result.Type3Routes {
		log.WithFields(log.Fields{
			"ip":    x.Route.IpAddress,
			"label": x.CommunityLocalAdmin,
		}).Trace("type-3 route")
	}

	return result, nil

}

// connect to gobgpd
func dial() (*grpc.ClientConn, api.GobgpApiClient, error) {

	conn, err := grpc.Dial(
		fmt.Sprintf("localhost:%d", Cfg.GobgpdPort), grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Error("failed to connect to GoBGP")
		return nil, nil, err
	}

	return conn, api.NewGobgpApiClient(conn), nil

}
