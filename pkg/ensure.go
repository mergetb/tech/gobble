package gobble

import (
	"net"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/rtnl"
	"golang.org/x/sys/unix"
)

// Ensure that the baselayer satisfies bgp's view of the world
func Ensure(bgpState *BgpState, baselayerState *BaselayerState) {

	// collect the current (from netlink) and desired (from bgp) forwarding maps
	// calculate what to add and remove, and update the baselayer
	currentFM, desiredFM := forwardingMaps(baselayerState, bgpState)
	add, remove := resolveFM(currentFM, desiredFM)

	err := AddNeighbors(add)
	if err != nil {
		log.WithError(err).Error("add neighbor")
	}
	err = RemoveNeighbors(remove)
	if err != nil {
		log.WithError(err).Error("remove neighbor")
	}

	// collect the current (from netlink) and desired (from bgp) routing tables
	// calculate what to add and remove, and update the baselayer
	currentRT, desiredRT := routingMaps(baselayerState, bgpState)
	addR, removeR := resolveRT(currentRT, desiredRT)

	err = AddRoutes(addR)
	if err != nil {
		log.WithError(err).Error("add route")
	}
	err = RemoveRoutes(removeR)
	if err != nil {
		log.WithError(err).Error("remove routes")
	}

}

func currentMaps(baselayer *BaselayerState, bgp *BgpState) map[string]Neighbor {

	current := make(map[string]Neighbor)

	// collect current forwarding entries from the baselayer

	for _, x := range baselayer.Neighborhood {

		// only consider entries that gobble manages
		if isGobbleEntry(x) {
			current[x.Key()] = x
		}

	}

	for _, x := range baselayer.Fib {
		for _, l := range baselayer.Links {

			// apply the vni of the neighbor interface to the neighbor
			if x.If == l.Index {
				x.Vni = l.Vni
			}

		}

		// only consider entries that gobble manages

		// NOTE This overwrites entries from the neighborhood, this is intentional.
		// The neighborhood actually includes the fib, but the fib has the family
		// value set to AF_BRIDGE and this becomes important later when setting FIB
		// entries as certain neighbors qualify to be AF_BRIDGE entires but not
		// AF_UNSPEC ones. There is almost certainly a better way to do this than
		// completely overlapping sets.

		if isGobbleEntry(x) {
			current[x.Key()] = x
		}
	}

	return current

}

func desiredMaps(baselayer *BaselayerState, bgp *BgpState) map[string]Neighbor {

	desired := make(map[string]Neighbor)

	// the peer router should always be there
	mac, err := baselayer.GetPeerMac()
	if err != nil {
		log.WithError(err).Error("could not determine peer router mac")
	} else {
		peer := Neighbor{
			Dst:    net.ParseIP(Cfg.PeerGw),
			If:     baselayer.GetPeerOif(),
			Mac:    mac,
			Family: unix.AF_INET,
		}
		desired[peer.Key()] = peer
	}

	// regular forwarding entries (non-BUM)
	for _, x := range bgp.Type2Routes {

		fields := log.Fields{"mac": x.Route.MacAddress}

		if len(x.Route.Labels) == 0 {
			log.WithFields(fields).Warn("type-2 route with no labels, ignoring")
		}

		for _, l := range x.Route.Labels {

			// bail on anything we don't have a vtep for
			vtep, ok := findVtep(l, baselayer)
			if !ok {
				log.WithFields(fields).Tracef("no vtep found for label %d", l)
				continue
			}

			for _, h := range x.NextHops {

				// don't add forwarding entries for local next-hops
				if baselayer.IsLocalIP(h) {
					continue
				}

				mac, err := net.ParseMAC(x.Route.MacAddress)
				if err != nil {
					log.WithError(err).WithFields(fields).Warning("failed to parse mac")
					continue
				}

				fe := Neighbor{
					Mac:    mac,
					Dst:    net.ParseIP(h),
					If:     vtep.Index,
					Family: unix.AF_BRIDGE,
				}
				desired[fe.Key()] = fe

			}

		}

	}

	// BUM forwarding entries

	for _, x := range bgp.Type3Routes {

		mac, _ := net.ParseMAC("00:00:00:00:00:00")

		// bail on anything we don't have a vtep for
		vtep, ok := findVtep(x.CommunityLocalAdmin, baselayer)
		if !ok {
			log.WithFields(log.Fields{
				"label": x.CommunityLocalAdmin,
			}).Trace("no vtep found for label")
			continue
		}

		ip := net.ParseIP(x.Route.IpAddress)
		// don't add forwarding entries for local next-hops
		if baselayer.IsLocalIP(ip.String()) {
			continue
		}

		fe := Neighbor{
			Mac:    mac,
			Dst:    net.ParseIP(x.Route.IpAddress),
			If:     vtep.Index,
			Family: unix.AF_BRIDGE,
		}
		desired[fe.Key()] = fe

	}

	return desired

}

// determine the set of current (from netlink) and desired forwarding maps. Maps
// are keyed based on the key functions for the Neighbor type as defined in
// baselayer.go
func forwardingMaps(baselayer *BaselayerState, bgp *BgpState) (
	map[string]Neighbor, map[string]Neighbor) {

	return currentMaps(baselayer, bgp), desiredMaps(baselayer, bgp)

}

// Collect the current (from netlink) and desired (from bgp) route maps. Maps
// are keyed based on the Route key function as defined in baselayer.go
func routingMaps(baselayer *BaselayerState, bgp *BgpState) (
	map[string]Route, map[string]Route) {

	current := make(map[string]Route)

	for _, x := range baselayer.DestRT {

		if x.Gateway.String() != Cfg.PeerGw {
			continue
		}
		current[x.Key()] = x

	}

	desired := make(map[string]Route)

	for _, x := range bgp.Type3Routes {

		if baselayer.IsLocalIP(x.Route.IpAddress) {
			log.Tracef("skipping local route %v", x.Route.IpAddress)
			continue
		}

		log.Tracef("found route %v", x.Route.IpAddress)

		rt := Route{
			Dest:    net.ParseIP(x.Route.IpAddress),
			Gateway: net.ParseIP(Cfg.PeerGw),
			Oif:     baselayer.GetPeerOif(),
		}
		desired[rt.Key()] = rt

	}

	return current, desired

}

// The following two functions compute symmetric differences across forwarding
// and routing tables. We need both functions b/c no generics in go
//	 all entries in desired not in current --> add
//	 all entries in current not in desired --> remove
func resolveFM(currentFM, desiredFM map[string]Neighbor) (
	[]Neighbor, []Neighbor) {

	var add, remove []Neighbor

	for key, desired := range desiredFM {
		_, ok := currentFM[key]
		if !ok {
			log.Infof("adding neighbor %s", key)
			add = append(add, desired)
		}
	}

	for key, current := range currentFM {
		_, ok := desiredFM[key]
		if !ok {
			log.Infof("removing neighbor %s", key)
			remove = append(remove, current)
		}
	}

	return add, remove

}

func resolveRT(currentRT, desiredRT map[string]Route) ([]Route, []Route) {

	var add, remove []Route

	for key, desired := range desiredRT {
		_, ok := currentRT[key]
		if !ok {
			log.Infof("adding route %s", key)
			add = append(add, desired)
		} else {
			log.Tracef("route exists %s", key)
		}
	}

	for key, current := range currentRT {
		_, ok := desiredRT[key]
		if !ok {
			log.Infof("removing route %s", key)
			remove = append(remove, current)
		}
	}

	return add, remove

}

// EnsureTableRules ensures that the IP routing policy rules are set up for the
// routing table Gobble presides over.
func EnsureTableRules() {

	peerGw := net.ParseIP(Cfg.PeerGw)
	if peerGw == nil {
		log.Fatal("bad peer-gateway")
	}

	_, bgpSubnet, err := net.ParseCIDR(Cfg.BgpSubnet)
	if err != nil {
		log.WithError(err).Fatal("bad bgp-subnet")
	}

	peerGwRule := &rtnl.Rule{
		Fib: rtnl.Fib{
			Family: unix.AF_INET,
			DstLen: 32,
		},
		Dest:     peerGw,
		Priority: 10,
		Table:    uint32(Cfg.Table),
	}

	bgpSubnetRule := &rtnl.Rule{
		Fib: rtnl.Fib{
			Family: unix.AF_INET,
			DstLen: 24,
		},
		Dest:     bgpSubnet.IP,
		Priority: 10,
		Table:    uint32(Cfg.Table),
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.WithError(err).Fatal("failed to get default context")
	}

	err = peerGwRule.Present(ctx)
	if err != nil {
		log.WithError(err).Fatal("failed to ensure peer gateway rule")
	}

	err = bgpSubnetRule.Present(ctx)
	if err != nil {
		log.WithError(err).Fatal("failed to ensure bgp subnet rule")
	}

}
